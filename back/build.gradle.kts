import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


buildscript {
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:2.0.5.RELEASE")
    }
}

plugins {
    idea
    id("org.jetbrains.kotlin.plugin.spring") version "1.3.30"
    id("org.jetbrains.kotlin.jvm") version "1.3.30"
    id("org.jetbrains.kotlin.plugin.jpa") version "1.3.30"
    id("org.jetbrains.kotlin.kapt") version "1.3.30"
    id("org.springframework.boot") version "2.1.5.RELEASE"
    id("io.spring.dependency-management") version "1.0.7.RELEASE"
}

group = "com.cm"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.jsonwebtoken:jjwt:0.2")
    implementation("io.jsonwebtoken:jjwt-impl:0.10.5")
    implementation("io.springfox:springfox-swagger2:2.6.1")
    implementation("io.springfox:springfox-swagger-ui:2.6.1")
    implementation("com.amazonaws:aws-java-sdk:1.11.133")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.boot:spring-boot-starter-mail")
    implementation("org.flywaydb:flyway-core")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.security:spring-security-jwt:1.0.2.RELEASE")
    runtimeOnly("org.postgresql:postgresql")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    compile("org.springframework.data:spring-data-commons")
    compile("javax.inject:javax.inject:1")
    compile("org.hibernate:hibernate-gradle-plugin:5.3.0.Final")
    compile("com.querydsl:querydsl-core:4.1.3")
    kapt("com.querydsl:querydsl-apt:4.1.3:jpa") // Magic happens here
    compile("com.querydsl:querydsl-jpa:4.1.3")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    compileOnly("org.springframework.boot:spring-boot-configuration-processor")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict") 
        jvmTarget = "1.8"
    }
}

idea {
    module {
        val kaptMain = file("build/generated/source/kapt/main")
        sourceDirs.add(kaptMain)
        generatedSourceDirs.add(kaptMain)
    }
}
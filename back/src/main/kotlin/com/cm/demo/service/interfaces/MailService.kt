package com.cm.demo.service.interfaces

import com.cm.demo.model.MailDto
import java.io.File

interface MailService {
    fun sendAcceptedMessage(mail: MailDto)
    fun sendRejectedMessage(mail: MailDto)
    fun sendAttachmentMessage(mail: MailDto, attachment: File)
}
package com.cm.demo.service.implementation

import com.cm.demo.model.UniversityExperience
import com.cm.demo.repository.UniversityExperienceRepository
import com.cm.demo.service.interfaces.UniversityExperienceService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UniversityExperienceServiceImpl(
        private val universityExperienceRepository: UniversityExperienceRepository) : UniversityExperienceService {

    @Transactional
    override fun addAll(universityExperiences: List<UniversityExperience>): List<UniversityExperience> {
        return this.universityExperienceRepository.saveAll(universityExperiences)
    }

    override fun findByCandidateId(id: Long): List<UniversityExperience> {
        return this.universityExperienceRepository.findByCandidateId(id)
    }

}
package com.cm.demo.service.interfaces

import com.cm.demo.dto.implementation.user.UserDtoWithToken
import com.cm.demo.payload.LoginRequest

interface LoginService {
    fun tokenEntity (loginRequest: LoginRequest) : UserDtoWithToken
}
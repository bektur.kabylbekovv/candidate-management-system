package com.cm.demo.service.implementation

import com.cm.demo.model.ReplaceNameDto
import com.cm.demo.service.interfaces.PlaceHoldersService
import org.springframework.stereotype.Service

@Service
class PlaceHoldersServiceImpl: PlaceHoldersService {

    override fun replacePlaceHolders(template : String, replaceNameDto : ReplaceNameDto) : String {
        var templateCopy: String = template

        templateCopy = templateCopy.replace("name_candidate", replaceNameDto.name_candidate, false)

        templateCopy = templateCopy.replace("candidate_id",    replaceNameDto.email, false)

        templateCopy = templateCopy.replace("givenDate", replaceNameDto.date, false)

        templateCopy = templateCopy.replace("text_hr", replaceNameDto.hrText, false)

        return templateCopy
    }
}
package com.cm.demo.service.implementation

import com.cm.demo.dto.implementation.user.UserDtoWithToken
import com.cm.demo.payload.LoginRequest
import com.cm.demo.security.JwtTokenProvider
import com.cm.demo.security.UserPrincipal
import com.cm.demo.service.interfaces.LoginService
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class LoginServiceImpl (val tokenProvider: JwtTokenProvider,
                        val authenticationManager: AuthenticationManager) : LoginService {

    override fun tokenEntity (loginRequest: LoginRequest) : UserDtoWithToken {
        val authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                        loginRequest.email,
                        loginRequest.password
                )
        )

        SecurityContextHolder.getContext().authentication = authentication

        val userPrincipal = authentication.principal as UserPrincipal

        val javaWebToken: String = tokenProvider.generateToken(authentication)

        return UserDtoWithToken(
                javaWebToken,
                userPrincipal.firstName,
                userPrincipal.lastName,
                userPrincipal.email,
                userPrincipal.role.toString())
    }
}
package com.cm.demo.service.implementation

import com.cm.demo.dto.implementation.user.CreateUserDto
import com.cm.demo.payload.ApiResponse
import com.cm.demo.service.interfaces.RegistrationService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class RegistrationServiceImpl (val userDetailsService: UserDetailsServiceImpl,
                               val passwordEncoder: PasswordEncoder) : RegistrationService {

    override fun registrationResponse(createUserDto: CreateUserDto): ResponseEntity<ApiResponse> {
        if (userDetailsService.existsByEmail(createUserDto.email)) {
            return ResponseEntity(ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.CONFLICT)
        }

        createUserDto.password = passwordEncoder.encode(createUserDto.password)

        userDetailsService.add(createUserDto)

        return ResponseEntity(ApiResponse(true, "User registered successfully"), HttpStatus.OK)
    }


}
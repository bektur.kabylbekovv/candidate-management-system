package com.cm.demo.service.implementation

import com.cm.demo.service.interfaces.LogoutService
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.stereotype.Service
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Service
class LogoutServiceImpl : LogoutService {
    override fun logoutResponse(request: HttpServletRequest, response: HttpServletResponse): String {
        val auth = SecurityContextHolder.getContext().authentication
        if (auth != null) {
            SecurityContextLogoutHandler().logout(request, response, auth)
            SecurityContextHolder.getContext().authentication = null
        }
        return "redirect:/login?logout"
    }
}
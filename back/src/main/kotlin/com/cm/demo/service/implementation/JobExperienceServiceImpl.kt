package com.cm.demo.service.implementation

import com.cm.demo.model.JobExperience
import com.cm.demo.repository.JobExperienceRepository
import com.cm.demo.service.interfaces.JobExperienceService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class JobExperienceServiceImpl(
        private val jobExperienceRepository: JobExperienceRepository) : JobExperienceService {

    @Transactional
    override fun addAll(jobExperiences: List<JobExperience>): List<JobExperience> {
        return this.jobExperienceRepository.saveAll(jobExperiences)
    }

    override fun findByCandidateId(id: Long): List<JobExperience> {
        return this.jobExperienceRepository.findByCandidateId(id)
    }


}
package com.cm.demo.service.interfaces

import com.cm.demo.model.ReplaceNameDto

interface PlaceHoldersService {
    fun replacePlaceHolders(template : String, replaceNameDto : ReplaceNameDto) : String
}
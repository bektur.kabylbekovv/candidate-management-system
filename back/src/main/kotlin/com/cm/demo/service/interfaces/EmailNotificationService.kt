package com.cm.demo.service.interfaces

import com.cm.demo.model.NotificationDto

interface EmailNotificationService {
    fun textSender(notificationDto: NotificationDto)
}
package com.cm.demo.service.interfaces

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

interface LogoutService {
    fun logoutResponse(request: HttpServletRequest, response: HttpServletResponse) : String
}
package com.cm.demo.service.interfaces

import com.cm.demo.model.JobExperience

interface JobExperienceService {

    fun findByCandidateId(id : Long) : List<JobExperience>

    fun addAll(jobExperiences: List<JobExperience>) : List<JobExperience>
}
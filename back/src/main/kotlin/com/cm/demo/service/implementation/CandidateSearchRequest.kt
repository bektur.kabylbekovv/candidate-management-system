package com.cm.demo.service.implementation

data class CandidateSearchRequest(
        var searchString: String?
)
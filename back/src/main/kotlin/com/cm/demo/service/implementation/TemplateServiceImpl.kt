package com.cm.demo.service.implementation

import com.cm.demo.repository.TemplateRepository
import com.cm.demo.service.interfaces.TemplateService
import org.springframework.stereotype.Service

@Service
class TemplateServiceImpl(val templateRepository: TemplateRepository) : TemplateService {

    override fun findTemplateByName(templateName: String) : String {
        return templateRepository.findTemplateByName(templateName).text
    }
}
package com.cm.demo.service.interfaces

import com.cm.demo.model.UniversityExperience

interface UniversityExperienceService {

    fun findByCandidateId(id: Long): List<UniversityExperience>

    fun addAll(universityExperiences: List<UniversityExperience>): List<UniversityExperience>
}
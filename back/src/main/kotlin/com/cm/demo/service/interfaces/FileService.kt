package com.cm.demo.service.interfaces

import org.springframework.web.multipart.MultipartFile
import java.io.File

interface FileService {
    fun convertMultiPartToFile(file: MultipartFile): File
}
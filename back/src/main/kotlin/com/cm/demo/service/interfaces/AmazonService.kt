package com.cm.demo.service.interfaces

import java.io.File

interface AmazonService {

    fun uploadPhotoFileToS3bucket(
            candidateEmail: String, originalFileName: String, file: File): String

    fun uploadSolutionFileToS3bucket(candidateEmail: String, originalFileName: String, file: File): String
}
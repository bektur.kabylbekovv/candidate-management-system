package com.cm.demo.service.implementation

import com.cm.demo.model.MailDto
import com.cm.demo.model.NotificationDto
import com.cm.demo.model.ReplaceNameDto
import com.cm.demo.service.interfaces.*
import org.springframework.stereotype.Service

@Service
class EmailNotificationServiceImpl(private val placeHoldersService: PlaceHoldersService,
                                   private val templateService: TemplateService,
                                   private val mailService: MailService,
                                   private val fileService: FileService) : EmailNotificationService {

   override fun textSender(notificationDto: NotificationDto){
       val candidateFullName: String = notificationDto.candidate.name + " " + notificationDto.candidate.surname

       val replacedText: String = placeHoldersService.replacePlaceHolders(
               templateService.findTemplateByName(notificationDto.templateName.name),
               ReplaceNameDto(
                       candidateFullName,
                       notificationDto.date,
                       notificationDto.hrText,
                       notificationDto.candidate.id,
                       notificationDto.candidate.email
               )
       )

       if(notificationDto.multipartFile != null){
           val convertedFile = fileService.convertMultiPartToFile(notificationDto.multipartFile)

           mailService.sendAttachmentMessage(
                   MailDto(
                           notificationDto.candidate.email,
                           "Zensoft",
                           replacedText,
                           notificationDto.candidate.id
                   ),
                   convertedFile
           )
           convertedFile.delete()
       }
       else
           mailService.sendAcceptedMessage(
               MailDto(
                       notificationDto.candidate.email,
                       "Zensoft",
                       replacedText,
                       notificationDto.candidate.id
               )
           )
    }
}
package com.cm.demo.service.interfaces

import com.cm.demo.model.University
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface UniversityService {

    fun findAll(name: String, pageable : Pageable): Page<University>

    fun findById(id : Long): University

    fun findAllByCountry(country : String, pageable: Pageable) : Page<University>

    fun add(university: University): University

    fun addAll(universities: List<University>): List<University>
}

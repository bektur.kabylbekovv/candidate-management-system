package com.cm.demo.service.implementation

import com.cm.demo.model.*
import com.cm.demo.model.enums.CandidateStatus
import com.cm.demo.repository.CandidateRepository
import com.cm.demo.service.interfaces.CandidateService
import com.querydsl.core.BooleanBuilder
import org.springframework.data.domain.*
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class CandidateServiceImpl(
        private val candidateRepository: CandidateRepository) : CandidateService {

    override fun findBydEmail(email: String): Candidate {
        return this.candidateRepository.findCandidateByEmail(email)
    }

    override fun findByIdAndEmail(id: Long, email: String): Candidate {
        return this.candidateRepository.findCandidateByIdAndEmail(id, email)
    }

    override fun findAll(name: String, pageable: Pageable): Page<Candidate> {
        if(name.isNotEmpty()) {
            return candidateRepository.findCandidateByNameOrSurnameIgnoreCase(name, pageable)
        }
        return this.candidateRepository.findAll(pageable)
    }

    override fun findById(id: Long): Candidate {
        return this.candidateRepository.findCandidateById(id)
    }

    override fun findByName(name: String, pageable: Pageable): Page<Candidate> {
        return this.candidateRepository.findCandidateByNameOrSurnameIgnoreCase(name, pageable)
    }

    @Transactional
    override fun add(candidate: Candidate): Candidate {
        return this.candidateRepository.save(candidate)
    }

    @Transactional
    override fun updateStatus(id: Long, status: CandidateStatus): String {
        val candidate: Candidate = this.candidateRepository.findCandidateById(id)
        candidate.candidateStatus = status
        this.candidateRepository.save(candidate)
        return status.toString()
    }

    @Transactional
    override fun forceUpdateStatus(id: Long, newStatus: CandidateStatus) {
        val candidate: Candidate = this.candidateRepository.findCandidateById(id)
        candidate.candidateStatus = newStatus
        this.candidateRepository.save(candidate)
    }

    @Transactional
    override fun deleteCandidateById(id: Long) {
        val candidate: Candidate = this.candidateRepository.findCandidateById(id)
        this.candidateRepository.delete(candidate)
    }

    override fun search(searchRequest: CandidateSearchRequest, pageable: Pageable): Page<Candidate> {

        val searchString: String? = searchRequest.searchString
        val predicate = BooleanBuilder()

        searchString?.let {
            val candidate = QCandidate.candidate
            predicate.apply {
                or(candidate.name.containsIgnoreCase(it))
                        .or(candidate.surname.containsIgnoreCase(it))
                        .or(candidate.email.containsIgnoreCase(it))
                        .or(candidate.department.name.containsIgnoreCase(it))
                        .or(candidate.phone.containsIgnoreCase(it))
                        .or(candidate.universities.any().university.name.containsIgnoreCase(it))
            }
            return this.candidateRepository.findAll(predicate, pageable)
        }
        return this.candidateRepository.findAll(pageable)
    }
}

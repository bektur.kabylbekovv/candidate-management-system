package com.cm.demo.service.implementation

import com.cm.demo.model.University
import com.cm.demo.repository.UniversityRepository
import com.cm.demo.service.interfaces.UniversityService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.SortDefault
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PathVariable

@Service
class UniversityServiceImpl(
        private val universityRepository : UniversityRepository) : UniversityService {

    override fun findAll(
            name: String, pageable : Pageable) : Page<University> {
        if(name.isNotEmpty()){
            return this.universityRepository.findUniversityByNameIgnoreCase(name, pageable)
        }
        return this.universityRepository.findAll(pageable)
    }

    override fun findById(id : Long) =
        this.universityRepository.findUniversityById(id)

    override fun findAllByCountry(
            @SortDefault(sort = ["country"])
            @PathVariable country : String, pageable: Pageable) =

        this.universityRepository.findUniversitiesByCountryIgnoreCase(country, pageable)

    @Transactional
    override fun add(university: University) : University {
        return this.universityRepository.save(university)
    }

    @Transactional
    override fun addAll(universities: List<University>) : List<University> {
        return this.universityRepository.saveAll(universities)
    }

}
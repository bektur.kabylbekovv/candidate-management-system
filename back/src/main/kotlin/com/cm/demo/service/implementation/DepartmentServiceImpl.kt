package com.cm.demo.service.implementation

import com.cm.demo.model.Department
import com.cm.demo.repository.DepartmentRepository
import com.cm.demo.service.interfaces.DepartmentService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class DepartmentServiceImpl (
        private val departmentRepository: DepartmentRepository) : DepartmentService {

    override fun findAll(
            name : String, pageable : Pageable) : Page<Department> {

        if(name.isNotEmpty()) {
            return this.departmentRepository.findDepartmentByNameIgnoreCase(name, pageable)
        }
        return this.departmentRepository.findAll(pageable)
    }

    override fun findById(id : Long) =
        this.departmentRepository.findDepartmentById(id)

    @Transactional
    override fun add(department: Department) : Department {
        return this.departmentRepository.save(department)
    }

    @Transactional
    override fun addAll(departments: List<Department>) : List<Department> {

        return this.departmentRepository.saveAll(departments)
    }

}
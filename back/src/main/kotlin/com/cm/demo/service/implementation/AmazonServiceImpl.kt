package com.cm.demo.service.implementation

import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.AmazonS3
import com.cm.demo.service.interfaces.AmazonService
import org.springframework.stereotype.Service
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.CannedAccessControlList
import java.io.File
import java.util.*
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.cm.demo.amazon.AmazonS3Credentials
import javax.annotation.PostConstruct

@Service
class AmazonServiceImpl(
        private val amazonS3Credentials: AmazonS3Credentials) : AmazonService {

    private lateinit var s3client: AmazonS3

    override fun uploadPhotoFileToS3bucket(
            candidateEmail: String, originalFileName: String, file: File): String {

        val uniquePhotoFileName: String = generateNewFileName(candidateEmail, originalFileName)
        s3client.putObject(PutObjectRequest(this.amazonS3Credentials.bucketName, uniquePhotoFileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead))
        return generateUrl(uniquePhotoFileName)
    }

    override fun uploadSolutionFileToS3bucket(
            candidateEmail: String, originalFileName: String, file: File): String {

        val uniqueFileName = "solution-${generateNewFileName(candidateEmail, originalFileName)}"
        s3client.putObject(PutObjectRequest(this.amazonS3Credentials.bucketName, uniqueFileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead))
        return generateUrl(uniqueFileName)
    }

    private fun generateNewFileName(candidateEmail: String, originalFileName: String): String {
        val date = Date().time.toString()
        return (date + candidateEmail.replace("@", "_")) +
                originalFileName.replace(" ", "_")
    }

    private fun generateUrl(fileName: String): String {
        return ("${amazonS3Credentials.endpointUrl}/${amazonS3Credentials.bucketName}/$fileName")
    }

    @PostConstruct
    private fun initializeAmazon() {
        val credentials = BasicAWSCredentials(this.amazonS3Credentials.accessKey, this.amazonS3Credentials.secretKey)
        this.s3client = AmazonS3ClientBuilder.standard().withRegion("eu-west-1")
                .withCredentials(AWSStaticCredentialsProvider(credentials))
                .build() as AmazonS3Client
    }
}
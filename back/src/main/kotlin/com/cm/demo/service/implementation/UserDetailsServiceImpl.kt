package com.cm.demo.service.implementation

import com.cm.demo.dto.implementation.user.CreateUserDto
import com.cm.demo.dto.interfaces.UserDtoMapper
import com.cm.demo.exceptions.ResourceNotFoundException
import com.cm.demo.model.enums.Role
import com.cm.demo.model.User
import com.cm.demo.repository.UserRepository
import com.cm.demo.security.UserPrincipal
import com.cm.demo.service.interfaces.UserService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.PathVariable
import java.util.*
import javax.transaction.Transactional

@Service
class UserDetailsServiceImpl(private val userRepository: UserRepository,
                             private val userDtoMapper: UserDtoMapper) : UserService {

    @Transactional
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findUserByEmail(username)
        return UserPrincipal.create(user)
    }

    @Transactional
    override fun loadUserById(id: Long): UserDetails {
        val user = userRepository.findById(id).orElseThrow { ResourceNotFoundException("User ID") }

        return UserPrincipal.create(user)
    }

    override fun findAll(pageable: Pageable): Page<User> {
        return this.userRepository.findAll(pageable)
    }

    @Transactional
    override fun add(createUserDto: CreateUserDto): User {
        return userRepository.save(userDtoMapper.convertToUserEntity(createUserDto))
    }

    override fun findUserById(@PathVariable id: Long): User {
        return this.userRepository.findUserById(id)
    }

    override fun findUserByEmail(@PathVariable email: String): User {
        return this.userRepository.findUserByEmail(email)
    }

    override fun existsByEmail(email: String): Boolean {
        return this.userRepository.existsByEmail(email)
    }

    @Transactional
    override fun changeRole(id: Long, newRole: Role) {
        val foundUser: User = this.userRepository.findUserById(id)
        if(foundUser.role == newRole) {
            return
        }
        foundUser.role = newRole
        this.userRepository.save(foundUser)
    }

    @Transactional
    override fun deleteUserById(id: Long) {
        val foundUser: User = this.userRepository.findUserById(id)
        if(foundUser.role == Role.ADMIN) {
            throw InputMismatchException("You can not delete user with role ADMIN")
        }
        this.userRepository.delete(foundUser)
    }
    
}
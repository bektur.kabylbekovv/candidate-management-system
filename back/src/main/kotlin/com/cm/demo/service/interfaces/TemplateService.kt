package com.cm.demo.service.interfaces

interface TemplateService {
    fun findTemplateByName(templateName : String) : String
}
package com.cm.demo.service.interfaces

import com.cm.demo.model.Department
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface DepartmentService {

    fun findAll(name: String, pageable: Pageable): Page<Department>

    fun findById(id: Long): Department

    fun add(department: Department) : Department

    fun addAll(departments: List<Department>) :  List<Department>
}
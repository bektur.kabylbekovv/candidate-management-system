package com.cm.demo.service.interfaces

import com.cm.demo.dto.implementation.user.CreateUserDto
import com.cm.demo.payload.ApiResponse
import org.springframework.http.ResponseEntity

interface RegistrationService {
    fun registrationResponse(createUserDto: CreateUserDto) : ResponseEntity<ApiResponse>
}
package com.cm.demo.service.implementation

import com.cm.demo.model.MailDto
import com.cm.demo.service.interfaces.MailService
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File

@Service
class MailServiceImpl (var sendMail: JavaMailSender): MailService {

    override fun sendAcceptedMessage(mail: MailDto) {
        val message = sendMail.createMimeMessage()
        val helper = MimeMessageHelper(message, false)
        helper.setTo(mail.email)
        helper.setSubject(mail.subject)
        helper.setText(mail.text)
        sendMail.send(message)
    }

    override fun sendRejectedMessage(mail: MailDto) {
        val message = sendMail.createMimeMessage()
        val helper = MimeMessageHelper(message, false)
        helper.setTo(mail.email)
        helper.setSubject(mail.subject)
        helper.setText(mail.text)
        sendMail.send(message)
    }

    override fun sendAttachmentMessage(mail: MailDto, attachment: File) {
        val message = sendMail.createMimeMessage()
        val helper = MimeMessageHelper(message, true)
        helper.setTo(mail.email)
        helper.setSubject(mail.subject)
        helper.setText(mail.text)
        helper.addAttachment(attachment.getName(), attachment)
        sendMail.send(message)
    }
}
package com.cm.demo.service.interfaces

import com.cm.demo.model.Candidate
import com.cm.demo.model.enums.CandidateStatus
import com.cm.demo.service.implementation.CandidateSearchRequest
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface CandidateService {

    fun findById(id: Long): Candidate

    fun findByIdAndEmail(id: Long, email: String): Candidate

    fun findAll(name: String, pageable: Pageable): Page<Candidate>

    fun add(candidate: Candidate): Candidate

    fun updateStatus(id: Long, status: CandidateStatus): String

    fun deleteCandidateById(id: Long)

    fun forceUpdateStatus(id: Long, newStatus: CandidateStatus)

    fun findBydEmail(email: String): Candidate

    fun search(searchRequest: CandidateSearchRequest, pageable: Pageable): Page<Candidate>

    fun findByName(name: String, pageable: Pageable): Page<Candidate>
}

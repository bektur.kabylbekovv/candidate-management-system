package com.cm.demo.service.interfaces

import com.cm.demo.dto.implementation.user.CreateUserDto
import com.cm.demo.model.User
import com.cm.demo.model.enums.Role
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.web.bind.annotation.PathVariable

interface UserService : UserDetailsService {

    fun loadUserById(id: Long): UserDetails
    fun findUserById(@PathVariable id: Long): User
    fun findAll(pageable: Pageable): Page<User>
    fun add(createUserDto: CreateUserDto): User
    fun findUserByEmail(@PathVariable email: String): User
    fun existsByEmail(email: String): Boolean
    fun changeRole(id: Long, newRole: Role)
    fun deleteUserById(id: Long)
}
package com.cm.demo.security

import com.cm.demo.model.enums.Role
import com.cm.demo.model.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*
import java.util.ArrayList

class UserPrincipal(
        val id: Long?,
        val firstName: String,
        val lastName: String,
        val email: String,
        val phone_number: String,
        private val password: String,
        var role: Role) : UserDetails {

    val ROLE_PREFIX: String = "ROLE_"

    override fun getUsername(): String {
        return email
    }

    override fun getPassword(): String {
        return password
    }

    override fun getAuthorities(): Collection<GrantedAuthority> {
        val roleList = ArrayList<GrantedAuthority>()
        roleList.add(SimpleGrantedAuthority(ROLE_PREFIX + role))

        return roleList
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id)
    }

    companion object comp_obj {

        fun create(user: User): UserPrincipal {
            return UserPrincipal(
                    user.id,
                    user.firstName,
                    user.lastName,
                    user.email,
                    user.phoneNumber,
                    user.password,
                    user.role
            )
        }
    }
}
package com.cm.demo.amazon

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class AmazonS3Credentials(

        @Value("\${amazonProperties.endpointUrl}")
        val endpointUrl: String,
        @Value("\${amazonProperties.bucketName}")
        val bucketName: String,
        @Value("\${amazonProperties.accessKey}")
        val accessKey: String,
        @Value("\${amazonProperties.secretKey}")
        val secretKey: String,
        @Value("\${amazonProperties.region}")
        val region: String)
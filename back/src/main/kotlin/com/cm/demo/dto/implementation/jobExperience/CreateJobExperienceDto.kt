package com.cm.demo.dto.implementation.jobExperience

class CreateJobExperienceDto(
        var jobTitle: String,
        var company: String,
        var startYear: Long,
        var endYear: Long,
        var descriptionOfJob: String
)
package com.cm.demo.dto.interfaces

import com.cm.demo.dto.implementation.candidate.CandidateDto
import com.cm.demo.dto.implementation.candidate.CreateCandidateDto
import com.cm.demo.dto.implementation.candidate.CandidatePreviewDto
import com.cm.demo.dto.implementation.candidate.PublicCandidateDto
import com.cm.demo.model.Candidate

interface CandidateDtoMapper {

    fun convertToDto(candidate: Candidate): CandidateDto

    fun convertToEntity(createCandidateDto: CreateCandidateDto): Candidate

    fun convertToCandidatePreviewDto(candidate: Candidate): CandidatePreviewDto
    fun convertToCandidateToPublicDto(candidate: Candidate): PublicCandidateDto

}

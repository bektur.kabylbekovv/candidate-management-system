package com.cm.demo.dto.implementation.user

import com.cm.demo.dto.interfaces.UserDtoMapper
import com.cm.demo.model.enums.Role
import com.cm.demo.model.User
import org.springframework.stereotype.Component

@Component
class UserDtoMapperMapperImpl : UserDtoMapper {


    override fun convertToUserDto(user: User) : UserDto {
        return UserDto(id = user.id, firstName = user.firstName,lastName = user.lastName,
                email = user.email,
                phoneNumber = user.phoneNumber, role = user.role.toString())
    }

    override fun convertToUserEntity(createUserDto: CreateUserDto) : User {
        return User(firstName = createUserDto.firstName,lastName = createUserDto.lastName ,
                email = createUserDto.email, phoneNumber = createUserDto.phoneNumber,
                givenPassword = createUserDto.password, role = Role.USER)
    }
}
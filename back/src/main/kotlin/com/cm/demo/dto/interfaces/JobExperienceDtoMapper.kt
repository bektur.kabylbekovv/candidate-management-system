package com.cm.demo.dto.interfaces

import com.cm.demo.dto.implementation.jobExperience.CreateJobExperienceDto
import com.cm.demo.dto.implementation.jobExperience.JobExperienceDto
import com.cm.demo.model.Candidate
import com.cm.demo.model.JobExperience

interface JobExperienceDtoMapper {

    fun convertToDto(jobExperience: JobExperience) : JobExperienceDto

    fun convertToEntity(createJobExperienceDto: CreateJobExperienceDto,
                        candidate: Candidate) : JobExperience
}
package com.cm.demo.dto.implementation.user

class UserDto (val id : Long?,
               val firstName: String,
               val lastName: String,
               val email: String,
               val phoneNumber: String,
               val role: String)
package com.cm.demo.dto.implementation.universityExperience

import com.cm.demo.dto.implementation.unversity.UniversityDto
import com.cm.demo.dto.interfaces.UniversityDtoMapper
import com.cm.demo.dto.interfaces.UniversityExperienceDtoMapper
import com.cm.demo.model.Candidate
import com.cm.demo.model.University
import com.cm.demo.model.UniversityExperience
import com.cm.demo.service.interfaces.UniversityService
import org.springframework.stereotype.Component

@Component
class UniversityExperienceDtoMapperImpl(
        private val universityService: UniversityService,
        private val universityDtoMapper: UniversityDtoMapper) : UniversityExperienceDtoMapper {

    override fun convertToDto(
            universityExperience: UniversityExperience): UniversityExperienceDto {

        val universityDto: UniversityDto =
                this.universityDtoMapper.convertToDto(universityExperience.university)

        return UniversityExperienceDto(
                university = universityDto,
                faculty = universityExperience.faculty,
                startYear = universityExperience.startYear,
                endYear = universityExperience.endYear
        )
    }

    override fun convertToEntity(
            createUniversityExperienceDto: CreateUniversityExperienceDto,
            candidate: Candidate): UniversityExperience {

        return UniversityExperience(
                university = this.universityService.findById(createUniversityExperienceDto.universityId),
                faculty = createUniversityExperienceDto.faculty,
                startYear = createUniversityExperienceDto.startYear,
                endYear = createUniversityExperienceDto.endYear,
                candidate = candidate
        )
    }
}
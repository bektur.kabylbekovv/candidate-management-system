package com.cm.demo.dto.implementation.universityExperience

import com.cm.demo.dto.implementation.unversity.UniversityDto

class UniversityExperienceDto(
        var university: UniversityDto? = null,
        var faculty: String,
        var startYear: Long,
        var endYear: Long
)
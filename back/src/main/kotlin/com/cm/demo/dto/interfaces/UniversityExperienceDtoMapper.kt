package com.cm.demo.dto.interfaces

import com.cm.demo.dto.implementation.universityExperience.CreateUniversityExperienceDto
import com.cm.demo.dto.implementation.universityExperience.UniversityExperienceDto
import com.cm.demo.model.Candidate
import com.cm.demo.model.UniversityExperience

interface UniversityExperienceDtoMapper {

    fun convertToDto(
            universityExperience: UniversityExperience): UniversityExperienceDto

    fun convertToEntity(
            createUniversityExperienceDto: CreateUniversityExperienceDto,
            candidate: Candidate): UniversityExperience
}

package com.cm.demo.dto.implementation.candidate

import com.cm.demo.dto.implementation.jobExperience.JobExperienceDto
import com.cm.demo.dto.implementation.universityExperience.UniversityExperienceDto
import com.cm.demo.model.enums.CandidateStatus
import java.time.LocalDate

class CandidateDto(
        var id: Long?,
        var name: String,
        var surname: String,
        var birthDate: LocalDate,
        var gender: Int,
        var familyStatus: Int,
        var candidateStatus: CandidateStatus,
        var email: String,
        var phone: String,
        var department: String,
        var applicationDate: LocalDate,
        var photoFileName: String?,
        var taskSolutionUrl: String?,
        var taskSolutionFileName: String?,
        var skills: String,
        var universityExperience: List<UniversityExperienceDto>?,
        var jobExperience: List<JobExperienceDto>?
)
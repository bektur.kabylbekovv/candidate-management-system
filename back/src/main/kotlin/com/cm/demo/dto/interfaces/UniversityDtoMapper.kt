package com.cm.demo.dto.interfaces

import com.cm.demo.dto.implementation.unversity.CreateUniversityDto
import com.cm.demo.dto.implementation.unversity.UniversityDto
import com.cm.demo.model.University

interface UniversityDtoMapper {

    fun convertToDto(university: University): UniversityDto

    fun convertToEntity(createUniversityDto: CreateUniversityDto): University
}
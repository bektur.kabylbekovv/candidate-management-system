package com.cm.demo.dto.implementation.universityExperience

class CreateUniversityExperienceDto(
        var universityId: Long,
        var faculty: String,
        var startYear: Long,
        var endYear: Long
)
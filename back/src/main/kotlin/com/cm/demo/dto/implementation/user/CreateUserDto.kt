package com.cm.demo.dto.implementation.user

import javax.validation.constraints.NotBlank

class CreateUserDto (val firstName: String,
                     val lastName: String,
                     @NotBlank val email: String,
                     val phoneNumber: String,
                     @NotBlank var password: String)
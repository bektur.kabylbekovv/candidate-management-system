package com.cm.demo.dto.implementation.jobExperience

import com.cm.demo.dto.interfaces.JobExperienceDtoMapper
import com.cm.demo.model.Candidate
import com.cm.demo.model.JobExperience
import org.springframework.stereotype.Component

@Component
class JobExperienceDtoMapperImpl : JobExperienceDtoMapper {

    override fun convertToDto(jobExperience: JobExperience): JobExperienceDto {
        return JobExperienceDto(
                jobTitle = jobExperience.jobTitle,
                company = jobExperience.company,
                startYear = jobExperience.startYear,
                endYear = jobExperience.endYear,
                descriptionOfJob = jobExperience.descriptionOfJob
        )
    }

    override fun convertToEntity(createJobExperienceDto: CreateJobExperienceDto,
                                 candidate: Candidate): JobExperience {
        return JobExperience(
                jobTitle = createJobExperienceDto.jobTitle,
                company = createJobExperienceDto.company,
                startYear = createJobExperienceDto.startYear,
                endYear = createJobExperienceDto.endYear,
                descriptionOfJob = createJobExperienceDto.descriptionOfJob,
                candidate = candidate
        )
    }
}
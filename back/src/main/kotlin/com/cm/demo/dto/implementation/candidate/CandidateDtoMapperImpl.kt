package com.cm.demo.dto.implementation.candidate

import com.cm.demo.dto.interfaces.CandidateDtoMapper
import com.cm.demo.dto.interfaces.UniversityExperienceDtoMapper
import com.cm.demo.model.Candidate
import com.cm.demo.model.Department
import com.cm.demo.model.enums.CandidateStatus
import com.cm.demo.service.interfaces.DepartmentService
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
class CandidateDtoMapperImpl(
        private val departmentService: DepartmentService) : CandidateDtoMapper {

    override fun convertToDto(candidate: Candidate): CandidateDto {
        return CandidateDto(
                id = candidate.id,
                name = candidate.name,
                surname = candidate.surname,
                birthDate = candidate.birthDate,
                phone = candidate.phone,
                email = candidate.email,
                gender = candidate.gender.ordinal,
                familyStatus = candidate.familyStatus.ordinal,
                candidateStatus = candidate.candidateStatus,
                department = candidate.department.name,
                applicationDate = candidate.applicationDate,
                taskSolutionFileName = candidate.testTaskSolutionFileName,
                taskSolutionUrl = candidate.testTaskSolutionUrl,
                photoFileName = candidate.photoUrl,
                universityExperience = null,
                jobExperience = null,
                skills = candidate.skills
        )
    }

    override fun convertToEntity(createCandidateDto: CreateCandidateDto): Candidate {
        val department: Department = this.departmentService.findById(createCandidateDto.departmentId)
        return Candidate(
                name = createCandidateDto.name,
                surname = createCandidateDto.surname,
                birthDate = createCandidateDto.birthDate,
                phone = createCandidateDto.phone,
                email = createCandidateDto.email,
                gender = createCandidateDto.gender,
                familyStatus = createCandidateDto.familyStatus,
                department = department,
                photoUrl = "",
                testTaskSolutionUrl = "",
                testTaskSolutionFileName = "Not Submitted",
                applicationDate = LocalDate.now(),
                candidateStatus = CandidateStatus.NEW,
                skills = createCandidateDto.skills,
                universities = emptyList()
        )
    }

    override fun convertToCandidateToPublicDto(candidate: Candidate): PublicCandidateDto {
        return PublicCandidateDto(
                id = candidate.id,
                name = candidate.name,
                surname = candidate.surname,
                birthDate = candidate.birthDate,
                phone = candidate.phone,
                email = candidate.email,
                gender = candidate.gender.ordinal,
                familyStatus = candidate.familyStatus.ordinal,
                candidateStatus = candidate.candidateStatus,
                department = candidate.department.name,
                applicationDate = candidate.applicationDate,
                taskSolutionFileName = candidate.testTaskSolutionFileName,
                taskSolutionUrl = candidate.testTaskSolutionUrl,
                photoFileName = candidate.photoUrl,
                universityExperience = null,
                jobExperience = null,
                skills = candidate.skills
        )
    }

    override fun convertToCandidatePreviewDto(candidate: Candidate): CandidatePreviewDto {
        return CandidatePreviewDto(
                id = candidate.id!!,
                name = candidate.name,
                surname = candidate.surname,
                gender = candidate.gender,
                phone = candidate.phone,
                email = candidate.email,
                department = candidate.department.name,
                photoFileName = candidate.photoUrl,
                candidateStatus = candidate.candidateStatus
        )
    }
}

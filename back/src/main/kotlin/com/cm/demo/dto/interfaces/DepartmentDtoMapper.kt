package com.cm.demo.dto.interfaces

import com.cm.demo.dto.implementation.department.DepartmentDto
import com.cm.demo.dto.implementation.department.CreateDepartmentDto
import com.cm.demo.model.Department

interface DepartmentDtoMapper {
    fun convertToDto(department: Department): DepartmentDto
    fun convertToEntity(createDepartmentDto: CreateDepartmentDto): Department
}




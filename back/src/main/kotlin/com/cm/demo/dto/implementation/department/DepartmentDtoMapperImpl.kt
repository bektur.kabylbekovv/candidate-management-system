package com.cm.demo.dto.implementation.department

import com.cm.demo.dto.interfaces.DepartmentDtoMapper
import com.cm.demo.model.Department

import org.springframework.stereotype.Component

@Component
class DepartmentDtoMapperImpl : DepartmentDtoMapper {

    override fun convertToDto(department: Department): DepartmentDto {
        return DepartmentDto(id = department.id, name = department.name)
    }

    override fun convertToEntity(createDepartmentDto: CreateDepartmentDto): Department {
        return Department(name = createDepartmentDto.name)
    }

}
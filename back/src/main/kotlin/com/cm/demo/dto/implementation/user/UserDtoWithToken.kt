package com.cm.demo.dto.implementation.user

class UserDtoWithToken (
        val accessToken: String,
        val firstName: String,
        val lastName: String,
        val email: String,
        val role: String)
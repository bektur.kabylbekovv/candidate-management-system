package com.cm.demo.dto.implementation.department


class DepartmentDto(
        val id : Long?,
        val name : String
)

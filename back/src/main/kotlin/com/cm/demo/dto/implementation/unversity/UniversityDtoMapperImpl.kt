package com.cm.demo.dto.implementation.unversity

import com.cm.demo.dto.interfaces.UniversityDtoMapper
import com.cm.demo.model.University
import org.springframework.stereotype.Component

@Component
class UniversityDtoMapperImpl : UniversityDtoMapper {

    override fun convertToDto(university: University): UniversityDto {
        return UniversityDto(id = university.id!!, name = university.name, country = university.country)
    }

    override fun convertToEntity(createUniversityDto: CreateUniversityDto): University {
        return University(name = createUniversityDto.name, country = createUniversityDto.country)
    }

}
package com.cm.demo.dto.implementation.candidate

import com.cm.demo.model.enums.CandidateStatus
import com.cm.demo.model.enums.Gender

class CandidatePreviewDto(
        var id: Long,
        var name: String,
        var surname: String,
        var gender: Gender,
        var phone: String,
        var department: String,
        var email: String,
        var photoFileName: String?,
        var candidateStatus: CandidateStatus
)
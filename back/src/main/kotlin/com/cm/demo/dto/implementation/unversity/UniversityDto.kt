package com.cm.demo.dto.implementation.unversity

class UniversityDto (
    val id: Long,
    val name : String,
    val country : String
)
package com.cm.demo.dto.implementation.unversity

class CreateUniversityDto (
        var name : String,
        val country : String
)
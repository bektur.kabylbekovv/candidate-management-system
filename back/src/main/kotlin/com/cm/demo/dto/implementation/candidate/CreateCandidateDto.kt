package com.cm.demo.dto.implementation.candidate

import com.cm.demo.model.enums.FamilyStatus
import com.cm.demo.model.enums.Gender
import com.cm.demo.dto.implementation.jobExperience.CreateJobExperienceDto
import com.cm.demo.dto.implementation.universityExperience.CreateUniversityExperienceDto
import org.hibernate.validator.constraints.Range
import java.time.LocalDate
import javax.validation.constraints.*

class  CreateCandidateDto(

        var gender: Gender,
        var familyStatus: FamilyStatus,

        @field:Pattern(regexp = "[A-Za-zА-Яа-яЁё]{2,20}",
                message = "Name must contain only alphabetical characters. " +
                          "Name must not be longer than 20 characters")
        var name: String,

        @field:Pattern(regexp = "[A-Za-zА-Яа-яЁё]{2,20}",
                message = "Surname must contain only alphabetical characters. " +
                          "Surname must not be longer than 20 characters or shorter than 2 characters")
        var surname: String,

        var birthDate: LocalDate,

        @field:Range(min = 17, max = 90, message = "Must be over eighteen years old")
        var age: Long = LocalDate.now().minusYears(birthDate.year.toLong()).year.toLong(),

        @field:Email
        @field:Pattern(regexp = "\\S+@\\S+\\.\\S+", message = "Invalid e-mail")
        var email: String,

        @field:NotBlank
        @field:Pattern(regexp = "\\+[-0-9 ]{12,17}",
                message = "Phone number must contain only digits and can not be longer than 17 digits or shorter than 12 digits")
        var phone: String,

        var departmentId: Long,
        var createUniversityExperienceDto: List<CreateUniversityExperienceDto>,
        var createJobExperienceDto: List<CreateJobExperienceDto>,

        var skills: String
)
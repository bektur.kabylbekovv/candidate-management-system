package com.cm.demo.dto.interfaces


import com.cm.demo.dto.implementation.user.CreateUserDto
import com.cm.demo.dto.implementation.user.UserDto
import com.cm.demo.model.User

interface UserDtoMapper {

    fun convertToUserDto(user: User) : UserDto

    fun convertToUserEntity(createUserDto: CreateUserDto) : User
}
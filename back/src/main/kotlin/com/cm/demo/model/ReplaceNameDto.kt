package com.cm.demo.model

class ReplaceNameDto (val name_candidate: String,
                      val date: String,
                      val hrText: String,
                      val id: Long?,
                      val email: String)
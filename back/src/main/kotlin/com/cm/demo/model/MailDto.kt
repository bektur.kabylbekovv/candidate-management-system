package com.cm.demo.model

class MailDto (
        val email: String,
        val subject: String,
        val text: String,
        val id: Long?)

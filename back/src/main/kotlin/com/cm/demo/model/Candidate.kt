package com.cm.demo.model

import com.cm.demo.model.enums.FamilyStatus
import com.cm.demo.model.enums.Gender
import com.cm.demo.model.enums.CandidateStatus
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "candidates")
class Candidate(

        @Enumerated(EnumType.STRING)
        @Column(name = "gender")
        var gender: Gender,

        @Enumerated(EnumType.STRING)
        @Column(name = "family_status")
        var familyStatus: FamilyStatus,

        @Enumerated(EnumType.STRING)
        @Column(name = "candidate_status")
        var candidateStatus: CandidateStatus,

        @Column(name = "name")
        var name: String,

        @Column(name = "surname")
        var surname: String,

        @Column(name = "email")
        var email: String,

        @Column(name = "phone")
        var phone: String,

        @ManyToOne
        @JoinColumn(name = "department_id")
        var department: Department,

        @Column(name = "birth_date")
        var birthDate: LocalDate,

        @Column(name = "application_date")
        var applicationDate: LocalDate,

        @Column(name = "photo_url")
        var photoUrl: String?,

        @Column(name = "task_solution_url")
        var testTaskSolutionUrl: String?,

        @Column(name = "task_solution_file_name")
        var testTaskSolutionFileName: String?,

        @Column(name = "skills")
        var skills: String,

        @OneToMany(mappedBy = "candidate")
        var universities: List<UniversityExperience>

) : BaseEntity()
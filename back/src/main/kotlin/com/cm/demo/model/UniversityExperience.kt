package com.cm.demo.model

import javax.persistence.*

@Entity
@Table(name = "university_experience")
class UniversityExperience(

        @ManyToOne
        @JoinColumn(name = "candidate_id")
        var candidate: Candidate,

        @ManyToOne
        @JoinColumn(name = "university_id")
        var university: University,

        @Column(name = "faculty")
        var faculty: String,

        @Column(name = "start_year")
        var startYear: Long,

        @Column(name = "end_year")
        var endYear: Long) : BaseEntity()


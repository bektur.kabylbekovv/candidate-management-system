package com.cm.demo.model.enums

enum class Gender {
    NOT_SPECIFIED,
    MALE,
    FEMALE
}
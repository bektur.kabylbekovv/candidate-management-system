package com.cm.demo.model

import javax.persistence.*

@Entity
@Table(name = "departments")
class Department(
        @Column(name = "name")
        var name: String

) : BaseEntity()

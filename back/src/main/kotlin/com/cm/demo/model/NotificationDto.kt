package com.cm.demo.model

import com.cm.demo.model.enums.TemplateName
import org.springframework.web.multipart.MultipartFile

class NotificationDto (val candidate: Candidate,
                       val multipartFile: MultipartFile?,
                       val date: String,
                       val hrText: String,
                       val templateName: TemplateName)
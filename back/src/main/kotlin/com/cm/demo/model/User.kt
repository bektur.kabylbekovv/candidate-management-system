package com.cm.demo.model

import com.cm.demo.model.enums.Role
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

@Entity(name = "User")
@Table(name = "new_users")
class User (
    @Column(name = "first_name")
    val firstName: String,

    @Column(name = "last_name")
    val lastName: String,

    @Email
    @NotEmpty
    @Column(name = "email")
    val email: String,

    @Column(name = "phone_number")
    val phoneNumber: String,

    @NotEmpty
    @Column(name = "password")
    val givenPassword: String,

    @NotBlank
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    var role: Role ) : UserDetails, BaseEntity() {

    override fun getAuthorities(): List<GrantedAuthority> {
        return emptyList()
    }

    override fun isEnabled(): Boolean {
        return true
    }

    override fun getUsername(): String {
        return this.email
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun getPassword(): String {
        return this.givenPassword
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }
}
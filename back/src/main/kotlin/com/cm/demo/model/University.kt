package com.cm.demo.model

import javax.persistence.*

@Entity
@Table(name = "universities")
class University(
        @Column(name = "name")
        var name: String,

        @Column(name = "country")
        var country: String) : BaseEntity()

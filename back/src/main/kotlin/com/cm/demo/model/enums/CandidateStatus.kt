package com.cm.demo.model.enums

enum class CandidateStatus {
    NEW,
    IN_PROGRESS,
    SENT_TASK,
    DECLINED,
    INVITED,
    ACCEPTED,
    IN_REVIEW,
}
package com.cm.demo.model.enums

enum class Role {
    ADMIN,
    USER,
    HR,
    INTERVIEWER
}
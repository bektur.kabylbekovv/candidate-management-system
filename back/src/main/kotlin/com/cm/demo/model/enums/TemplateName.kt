package com.cm.demo.model.enums

enum class TemplateName {
    NEW,
    IN_PROGRESS,
    SENT_TASK,
    DECLINED,
    INVITED,
    ACCEPTED,
    IN_REVIEW,
}

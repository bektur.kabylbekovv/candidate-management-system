package com.cm.demo.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "templates")
class Template(
        @Column(name = "name")
        val name: String,

        @Column(name = "text")
        val text: String) : BaseEntity()

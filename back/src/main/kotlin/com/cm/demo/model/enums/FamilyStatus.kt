package com.cm.demo.model.enums

enum class FamilyStatus {
    NOT_SPECIFIED,
    MARRIED,
    NOT_MARRIED,
    DEVORCED,
}
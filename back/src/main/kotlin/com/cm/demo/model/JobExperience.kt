package com.cm.demo.model

import javax.persistence.*

@Entity
@Table(name = "job_experience")
class JobExperience(
        @ManyToOne
        @JoinColumn(name = "candidate_id")
        var candidate: Candidate,

        @Column(name = "job_title")
        var jobTitle: String,

        @Column(name = "company")
        var company: String,

        @Column(name = "start_year")
        var startYear: Long,

        @Column(name = "end_year")
        var endYear: Long,

        @Column(name = "description")
        var descriptionOfJob: String) : BaseEntity()
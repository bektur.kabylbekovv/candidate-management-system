package com.cm.demo.controller

import com.cm.demo.dto.implementation.user.UserDto
import com.cm.demo.endpoint.interfaces.CandidateEndpoint
import com.cm.demo.endpoint.interfaces.UserEndpoint
import com.cm.demo.model.enums.CandidateStatus
import com.cm.demo.model.enums.Role
import io.swagger.annotations.ApiOperation
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/admin")
@PreAuthorize("hasRole('ROLE_ADMIN')")
class AdminController(
        private val userEndpoint: UserEndpoint,
        private val candidateEndpoint: CandidateEndpoint) {

    @GetMapping("/users")
    @ApiOperation("Returns all users")
    fun getAll(pageable: Pageable): Page<UserDto> {
        return this.userEndpoint.getAll(pageable)
    }

    @PutMapping("user/{id}/role")
    @ApiOperation("Change role of user to provided Role")
    fun changeRoleToHR(@PathVariable id: Long, newRole: Role) {
        return this.userEndpoint.changeRoleById(id, newRole)
    }

    @PutMapping("/candidates/{id}/status")
    @ApiOperation("Changes status of candidate ignoring state machine logic")
    fun forceChangeCandidateStatus(@PathVariable id: Long, newStatus: CandidateStatus) {
        this.candidateEndpoint.forceChangeCandidateStatus(id, newStatus)
    }

    @DeleteMapping("user/{id}")
    @ApiOperation("Deletes user by provided id")
    fun deleteUser(@PathVariable id: Long) {
        return this.userEndpoint.deleteUserById(id)
    }

    @DeleteMapping("/candidates/{id}")
    @ApiOperation("Deletes candidate by provided id")
    fun deleteCandidate(@PathVariable id: Long) {
        return this.candidateEndpoint.deleteCandidateById(id)
    }

}
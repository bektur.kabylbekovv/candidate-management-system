package com.cm.demo.controller

import com.cm.demo.dto.implementation.unversity.CreateUniversityDto
import com.cm.demo.endpoint.interfaces.UniversityEndpoint
import io.swagger.annotations.ApiOperation
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api/universities")
class UniversityController(private val universityEndpoint: UniversityEndpoint) {

    @ApiOperation(value = "Creates University and Returns it")
    @PostMapping
    @PreAuthorize("hasRole('ROLE_HR') or hasRole('ROLE_INTERVIEWER') or hasRole('ROLE_ADMIN')")
    fun add(@RequestBody createUniversityDto: CreateUniversityDto) =
            this.universityEndpoint.add(createUniversityDto)

    @ApiOperation(value = "Creates multiple Universities and Returns them")
    @PostMapping("/addAll")
    @PreAuthorize("hasRole('ROLE_HR') or hasRole('ROLE_INTERVIEWER') or hasRole('ROLE_ADMIN')")
    fun addAll(@RequestBody createUniversitiesDto: List<CreateUniversityDto>) =
            this.universityEndpoint.addAll(createUniversitiesDto)

}
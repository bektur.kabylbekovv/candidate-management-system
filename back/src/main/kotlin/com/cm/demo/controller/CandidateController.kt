package com.cm.demo.controller

import com.cm.demo.dto.implementation.candidate.CandidateDto
import com.cm.demo.dto.implementation.candidate.CandidatePreviewDto
import com.cm.demo.endpoint.interfaces.CandidateEndpoint
import com.cm.demo.service.implementation.CandidateSearchRequest
import io.swagger.annotations.ApiOperation
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api/candidates")
class CandidateController(
        private val candidateEndpoint: CandidateEndpoint) {

    @ApiOperation(value = "Returns All Candidates")
    @GetMapping
    @PreAuthorize("hasRole('ROLE_HR') or hasRole('ROLE_ADMIN') or hasRole('ROLE_INTERVIEWER')")
    fun getAll(name: String, pageable: Pageable): Page<CandidateDto> =
            this.candidateEndpoint.getAll(name, pageable)

    @ApiOperation(value = "Returns Candidate by id")
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_HR') or hasRole('ROLE_ADMIN') or hasRole('ROLE_INTERVIEWER')")
    fun getById(@PathVariable id: Long): CandidateDto {
        return this.candidateEndpoint.getById(id)
    }

    @ApiOperation(value = "Returns simplified Candidate for page with list of Candidates")
    @GetMapping("/simplified")
    @PreAuthorize("hasRole('ROLE_HR') or hasRole('ROLE_ADMIN') or hasRole('ROLE_INTERVIEWER')")
    fun getAllForPreview(pageable: Pageable): Page<CandidatePreviewDto> =
            this.candidateEndpoint.getAllForPreview(pageable)

    @ApiOperation(value = "Changes candidate's status to INVITED_TO_INTERVIEW")
    @PutMapping("/{id}/invite")
    @PreAuthorize("hasRole('ROLE_HR') or hasRole('ROLE_ADMIN')")
    fun changeStatusToInvited(@PathVariable id: Long, @RequestParam file: MultipartFile?,
                              @RequestParam date: String, @RequestParam hrText: String?) {
        this.candidateEndpoint.changeStatusToInvited(id, date)
    }

    @ApiOperation(value = "Changes candidate's status to RECIEVED_TEST_ASSIGNMENT")
    @PutMapping("/{id}/send_task")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_INTERVIEWER')")
    fun changeStatusToSentTask(@PathVariable id: Long, @RequestParam file: MultipartFile,
                               @RequestParam date: String, @RequestParam hrText: String?) {
        this.candidateEndpoint.changeStatusToSentTask(id, file, date)
    }

    @ApiOperation(value = "Changes candidate's status to DECLINED")
    @PutMapping("/{id}/decline")
    @PreAuthorize("hasRole('ROLE_HR') or hasRole('ROLE_ADMIN') or hasRole('ROLE_INTERVIEWER')")
    fun changeStatusToDeclined(@PathVariable id: Long, @RequestParam file: MultipartFile?,
                               @RequestParam date: String?, @RequestParam hrText: String) {
        this.candidateEndpoint.changeStatusToDeclined(id, hrText)
    }

    @ApiOperation(value = "Changes candidate's status to ACCEPTED")
    @PutMapping("/{id}/accept")
    @PreAuthorize("hasRole('ROLE_HR') or hasRole('ROLE_ADMIN')")
    fun changeStatusToAccepted(@PathVariable id: Long, @RequestParam file: MultipartFile?,
                               @RequestParam date: String, @RequestParam hrText: String?) {
        this.candidateEndpoint.changeStatusToAccepted(id, date)
    }

    @ApiOperation(value = "Changes candidate's status to IN_PROGRESS")
    @PutMapping("/{id}/in_progress")
    @PreAuthorize("hasRole('ROLE_HR') or hasRole('ROLE_ADMIN')")
    fun changeStatusToInProgress(@PathVariable id: Long) {
        this.candidateEndpoint.changeStatusToInProgress(id)
    }

    @ApiOperation(value = "Changes candidate's status to IN_PROGRESS")
    @PutMapping("/{id}/review")
    @PreAuthorize("hasRole('ROLE_HR') or hasRole('ROLE_ADMIN')")
    fun changeStatusToInReview(@PathVariable id: Long, @RequestParam file: MultipartFile?,
                               @RequestParam date: String?, @RequestParam hrText: String?) {
        this.candidateEndpoint.changeStatusToInReview(id)
    }

    @ApiOperation(value = "Searches for candidate by provided string pattern")
    @GetMapping("/search")
    fun search(@RequestBody searchRequest: CandidateSearchRequest, pageable: Pageable): Page<CandidatePreviewDto> {
        return this.candidateEndpoint.search(searchRequest, pageable)
    }
}


package com.cm.demo.controller

import com.cm.demo.service.interfaces.LogoutService
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.*
import javax.annotation.security.PermitAll
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpServletRequest


@RestController
@RequestMapping("/api/user")
@PermitAll
class AuthController(
        val logoutService: LogoutService) {

    @ApiOperation("Logs user out")
    @GetMapping("/logout")
    fun logoutUser(request: HttpServletRequest, response: HttpServletResponse): String {
        return logoutService.logoutResponse(request, response)
    }

}

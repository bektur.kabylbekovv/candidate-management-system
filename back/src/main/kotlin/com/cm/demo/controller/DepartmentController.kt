package com.cm.demo.controller

import com.cm.demo.dto.implementation.department.CreateDepartmentDto
import com.cm.demo.endpoint.interfaces.DepartmentEndpoint
import io.swagger.annotations.ApiOperation
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api/departments")
class DepartmentController(private val departmentEndpoint: DepartmentEndpoint) {

    @ApiOperation(value = "Creates Department and Returns it")
    @PostMapping
    @PreAuthorize("hasRole('ROLE_HR') or hasRole('ROLE_INTERVIEWER') or hasRole('ROLE_ADMIN')")
    fun add(@RequestBody createDepartmentDto: CreateDepartmentDto) =
            departmentEndpoint.add(createDepartmentDto)

    @ApiOperation(value = "Creates multiple Departments and Returns them")
    @PostMapping("/addAll")
    @PreAuthorize("hasRole('ROLE_HR') or hasRole('ROLE_INTERVIEWER') or hasRole('ROLE_ADMIN')")
    fun addAll(@RequestBody createDepartmentsDto: List<CreateDepartmentDto>) =
            departmentEndpoint.addAll(createDepartmentsDto)
}
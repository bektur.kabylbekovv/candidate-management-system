package com.cm.demo.controller

import com.cm.demo.dto.implementation.candidate.CandidateDto
import com.cm.demo.dto.implementation.candidate.CreateCandidateDto
import com.cm.demo.dto.implementation.candidate.PublicCandidateDto
import com.cm.demo.dto.implementation.user.CreateUserDto
import com.cm.demo.dto.implementation.user.UserDtoWithToken
import com.cm.demo.endpoint.implementation.FileEndpointImpl
import com.cm.demo.endpoint.interfaces.CandidateEndpoint
import com.cm.demo.endpoint.interfaces.DepartmentEndpoint
import com.cm.demo.endpoint.interfaces.UniversityEndpoint
import com.cm.demo.payload.LoginRequest
import com.cm.demo.service.interfaces.LoginService
import com.cm.demo.service.interfaces.RegistrationService
import io.swagger.annotations.ApiOperation
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("/api/public")
class PublicApiController(
        private val candidateEndpoint: CandidateEndpoint,
        private val universityEndpoint: UniversityEndpoint,
        private val fileEndpoint: FileEndpointImpl,
        private val departmentEndpoint: DepartmentEndpoint,
        private val registrationService: RegistrationService,
        val loginService: LoginService) {

    @ApiOperation("Registration")
    @PostMapping("/registration")
    fun registerUser(@Validated @RequestBody createUserDto: CreateUserDto): ResponseEntity<*> {
        return registrationService.registrationResponse(createUserDto)
    }

    @ApiOperation("Logs user in")
    @PostMapping("/login")
    fun loginUser(@Validated @RequestBody loginRequest: LoginRequest): UserDtoWithToken {
        return loginService.tokenEntity(loginRequest)
    }

    @ApiOperation(value = "Creates Candidate and Returns it")
    @PostMapping("/candidates")
    fun addCandidate(@RequestBody @Validated createCandidateDto: CreateCandidateDto): CandidateDto =
            this.candidateEndpoint.add(createCandidateDto)

    @ApiOperation(value = "Uploads photo and returns link to get it from AWS S3 Bucket")
    @PostMapping("/candidates/photo")
    fun addPhotoToCandidate(candidateId: Long, candidateEmail: String, candidatePhoto: MultipartFile): String =
            this.fileEndpoint.addPhoto(candidateId, candidateEmail, candidatePhoto)

    @ApiOperation(value = "Uploads solution file and returns link to get it from AWS S3 Bucket")
    @PostMapping("/candidates/submit-solution")
    fun addTaskSolutionToCandidate(candidateId: Long, candidateEmail: String, candidatePhoto: MultipartFile): String =
            this.fileEndpoint.addSolution(candidateId, candidateEmail, candidatePhoto)

    @ApiOperation(value = "Returns list of all Universities")
    @GetMapping("/universities")
    fun getAllUniversities(name: String, pageable: Pageable) =
            this.universityEndpoint.getAll(name, pageable)

    @ApiOperation(value = "Returns all Departments")
    @GetMapping("/departments")
    fun getAllDepartments(name: String, pageable: Pageable) =
            this.departmentEndpoint.getAll(name, pageable)

    @GetMapping("/candidate/info")
    @ApiOperation(value = "Returns simplified information about candidate")
    fun getById(@RequestParam email: String): PublicCandidateDto {
        return this.candidateEndpoint.getByEmail(email)
    }

}
package com.cm.demo.payload
import javax.validation.constraints.NotBlank

class LoginRequest (@NotBlank val email: String,
                    @NotBlank val password: String)

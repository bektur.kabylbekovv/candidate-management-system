package com.cm.demo.handler

import org.springframework.http.HttpStatus

class ExceptionResponseDto(
        var code: HttpStatus,
        var message: String
)
package com.cm.demo.handler

import com.cm.demo.exceptions.ResourceNotFoundException
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import org.hibernate.exception.ConstraintViolationException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.MethodArgumentNotValidException

interface ExceptionToDtoMapper {
    fun mapMethodArgumentNotValidException(ex: MethodArgumentNotValidException, code: HttpStatus): ExceptionResponseDto

    fun mapConstraintViolationException(ex: ConstraintViolationException, code: HttpStatus): ExceptionResponseDto

    fun mapInvalidFormatException(ex: InvalidFormatException, code: HttpStatus): ExceptionResponseDto

    fun mapEmptyResultDataAccessException(ex: EmptyResultDataAccessException, code: HttpStatus): ExceptionResponseDto

    fun mapResourceNotFoundException(ex: ResourceNotFoundException, code: HttpStatus): ExceptionResponseDto

    fun mapMissingKotlinParameterException(ex: MissingKotlinParameterException, code: HttpStatus): ExceptionResponseDto
}

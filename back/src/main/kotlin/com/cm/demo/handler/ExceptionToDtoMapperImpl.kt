package com.cm.demo.handler

import com.cm.demo.exceptions.ResourceNotFoundException
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import org.hibernate.exception.ConstraintViolationException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.bind.MethodArgumentNotValidException

@Component
class ExceptionToDtoMapperImpl : ExceptionToDtoMapper {

    override fun mapMethodArgumentNotValidException(ex: MethodArgumentNotValidException, code: HttpStatus): ExceptionResponseDto {
        return ExceptionResponseDto(code = code, message = ex.message)
    }

    override fun mapConstraintViolationException(ex: ConstraintViolationException, code: HttpStatus): ExceptionResponseDto {
        val invalidValue = ex.sqlException.localizedMessage
                .substringAfter("=(")
                .substringBefore(")")
        val message = "$invalidValue is already used"
        return ExceptionResponseDto(code = code, message = message)
    }

    override fun mapInvalidFormatException(ex: InvalidFormatException, code: HttpStatus): ExceptionResponseDto {
        val field = ex.targetType.simpleName
        val possibleValues = ex.targetType.fields.joinToString { it.name }
        var message = "Only following values are allowed for $field: $possibleValues."
        if (field == "LocalDate") {
            message = "Invalid date of birth."
        }
        return ExceptionResponseDto(code = code, message = message)
    }

    override fun mapEmptyResultDataAccessException(ex: EmptyResultDataAccessException, code: HttpStatus): ExceptionResponseDto {
        val message = "Provided ID is not in acceptable range"
        return ExceptionResponseDto(code = code, message = message)
    }

    override fun mapResourceNotFoundException(ex: ResourceNotFoundException, code: HttpStatus): ExceptionResponseDto {
        return ExceptionResponseDto(code = code, message = ex.localizedMessage)
    }

    override fun mapMissingKotlinParameterException(ex: MissingKotlinParameterException, code: HttpStatus): ExceptionResponseDto {
        val message = "${ex.parameter.name!!} is missing"
        return ExceptionResponseDto(code = code, message = message)
    }
}
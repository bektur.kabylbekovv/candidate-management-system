package com.cm.demo.handler

import com.cm.demo.exceptions.ResourceNotFoundException
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import org.hibernate.exception.ConstraintViolationException
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
class ErrorHandlingController(
        private val exceptionToToDtoMapper: ExceptionToDtoMapper) {

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(MethodArgumentNotValidException::class)
    internal fun validationError(ex: MethodArgumentNotValidException): ExceptionResponseDto =
            this.exceptionToToDtoMapper.mapMethodArgumentNotValidException(ex, HttpStatus.UNPROCESSABLE_ENTITY)

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(ConstraintViolationException::class)
    internal fun emailIsAlreadyUsed(ex: ConstraintViolationException): ExceptionResponseDto =
            this.exceptionToToDtoMapper.mapConstraintViolationException(ex, HttpStatus.UNPROCESSABLE_ENTITY)

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidFormatException::class)
    internal fun invalidValueForEnum(ex: InvalidFormatException): ExceptionResponseDto =
            this.exceptionToToDtoMapper.mapInvalidFormatException(ex, HttpStatus.BAD_REQUEST)

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(EmptyResultDataAccessException::class)
    internal fun idIsNotInAcceptableRange(ex: EmptyResultDataAccessException): ExceptionResponseDto =
            this.exceptionToToDtoMapper.mapEmptyResultDataAccessException(ex, HttpStatus.BAD_REQUEST)

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ResourceNotFoundException::class)
    internal fun resourceNotFoundHandler(ex: ResourceNotFoundException): ExceptionResponseDto =
            this.exceptionToToDtoMapper.mapResourceNotFoundException(ex, HttpStatus.BAD_REQUEST)

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MissingKotlinParameterException::class)
    internal fun missingKotlinParameterHandler(ex: MissingKotlinParameterException): ExceptionResponseDto =
            this.exceptionToToDtoMapper.mapMissingKotlinParameterException(ex, HttpStatus.BAD_REQUEST)


}
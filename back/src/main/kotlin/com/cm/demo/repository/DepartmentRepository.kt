package com.cm.demo.repository

import com.cm.demo.model.Department
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

interface DepartmentRepository :  JpaRepository<Department, Long> {

    fun findDepartmentById(id : Long) : Department

    fun findDepartmentByNameIgnoreCase(name :String, pageable: Pageable) : Page<Department>

}
package com.cm.demo.repository

import com.cm.demo.model.University
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

interface UniversityRepository : JpaRepository<University, Long> {

    fun findUniversityById(id : Long) : University

    fun findUniversityByNameIgnoreCase(name : String, pageable: Pageable) : Page<University>

    fun findUniversitiesByCountryIgnoreCase(country : String, pageable : Pageable) : Page<University>

}

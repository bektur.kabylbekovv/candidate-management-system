package com.cm.demo.repository

import com.cm.demo.model.JobExperience
import org.springframework.data.jpa.repository.JpaRepository

interface JobExperienceRepository : JpaRepository<JobExperience, Long> {

    fun findByCandidateId(id: Long) : List<JobExperience>


}
package com.cm.demo.repository

import com.cm.demo.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<User, Long> {

    fun findUserById(id : Long) : User

    fun findUserByEmail(email : String) : User

    fun existsByEmail(email: String): Boolean
}
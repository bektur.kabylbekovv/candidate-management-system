package com.cm.demo.repository

import com.cm.demo.model.Candidate
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.querydsl.QuerydslPredicateExecutor
import org.springframework.data.jpa.repository.JpaRepository


interface CandidateRepository : JpaRepository<Candidate, Long>, QuerydslPredicateExecutor<Candidate> {

    fun findCandidateById(id: Long): Candidate

    fun findCandidateByIdAndEmail(id: Long, email: String): Candidate

    fun findCandidateByEmail(email: String): Candidate

    fun findCandidateByNameOrSurnameIgnoreCase(nameOrSurname: String, pageable: Pageable): Page<Candidate>

}

package com.cm.demo.repository

import com.cm.demo.model.Template
import org.springframework.data.jpa.repository.JpaRepository

interface TemplateRepository : JpaRepository<Template, Long> {

    fun findTemplateById(id : Long) : Template

    fun findTemplateByName(name : String) : Template
}
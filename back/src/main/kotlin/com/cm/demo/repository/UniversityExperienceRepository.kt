package com.cm.demo.repository

import com.cm.demo.model.UniversityExperience
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor

interface UniversityExperienceRepository: JpaRepository<UniversityExperience, Long>,
         QuerydslPredicateExecutor<UniversityExperience> {

    fun findByCandidateId(candidateId: Long): List<UniversityExperience>

}
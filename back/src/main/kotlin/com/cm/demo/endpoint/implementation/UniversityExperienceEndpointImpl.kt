package com.cm.demo.endpoint.implementation

import com.cm.demo.dto.implementation.universityExperience.CreateUniversityExperienceDto
import com.cm.demo.dto.implementation.universityExperience.UniversityExperienceDto
import com.cm.demo.dto.interfaces.UniversityExperienceDtoMapper
import com.cm.demo.endpoint.interfaces.UniversityExperienceEndpoint
import com.cm.demo.model.Candidate
import com.cm.demo.model.UniversityExperience
import com.cm.demo.service.interfaces.UniversityExperienceService
import org.springframework.stereotype.Component

@Component
class UniversityExperienceEndpointImpl(
        private val universityExperienceService: UniversityExperienceService,
        private val universityExperienceDtoMapper: UniversityExperienceDtoMapper
) : UniversityExperienceEndpoint {

    override fun getUniversityExperienceByCandidateId(candidateId: Long): List<UniversityExperienceDto> {
        val foundUniversityExperiences: List<UniversityExperience> =
                this.universityExperienceService.findByCandidateId(candidateId)

        return foundUniversityExperiences
                .map { this.universityExperienceDtoMapper.convertToDto(it) }

    }

    override fun addAllUniversityExperience(
            createUniversityExperiencesDto: List<CreateUniversityExperienceDto>,
            candidate: Candidate): List<UniversityExperienceDto> {

        val universityExperiences: List<UniversityExperience> = createUniversityExperiencesDto
                .map { this.universityExperienceDtoMapper.convertToEntity(it, candidate) }

        val createdUniversityExperiences: List<UniversityExperience> =
                this.universityExperienceService.addAll(universityExperiences)

        return createdUniversityExperiences.map { this.universityExperienceDtoMapper.convertToDto(it) }
    }
}
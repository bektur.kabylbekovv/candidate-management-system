package com.cm.demo.endpoint.interfaces

import com.cm.demo.dto.implementation.jobExperience.CreateJobExperienceDto
import com.cm.demo.dto.implementation.jobExperience.JobExperienceDto
import com.cm.demo.model.Candidate

interface JobExperienceEndPoint {
    fun getJobExperienceByCandidateId(id: Long) : List<JobExperienceDto>

    fun addAllJobExperience(
            createJobExperiencesDto: List<CreateJobExperienceDto>,
            candidate: Candidate) : List<JobExperienceDto>
}
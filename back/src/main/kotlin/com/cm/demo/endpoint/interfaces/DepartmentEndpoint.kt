package com.cm.demo.endpoint.interfaces

import com.cm.demo.dto.implementation.department.DepartmentDto
import com.cm.demo.dto.implementation.department.CreateDepartmentDto
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface DepartmentEndpoint {

    fun getAll(name: String, pageable: Pageable): Page<DepartmentDto>

    fun getById(id: Long): DepartmentDto

    fun add(createDepartmentDto: CreateDepartmentDto): DepartmentDto

    fun addAll(createDepartmentsDto: List<CreateDepartmentDto>): List<DepartmentDto>

}
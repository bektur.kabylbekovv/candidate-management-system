package com.cm.demo.endpoint.implementation

import com.cm.demo.dto.implementation.candidate.CandidateDto
import com.cm.demo.dto.implementation.candidate.CreateCandidateDto
import com.cm.demo.dto.implementation.candidate.CandidatePreviewDto
import com.cm.demo.dto.implementation.candidate.PublicCandidateDto
import com.cm.demo.dto.implementation.jobExperience.JobExperienceDto
import com.cm.demo.dto.implementation.universityExperience.UniversityExperienceDto
import com.cm.demo.dto.interfaces.CandidateDtoMapper
import com.cm.demo.endpoint.interfaces.*
import com.cm.demo.exceptions.InvalidAdressException
import com.cm.demo.model.Candidate
import com.cm.demo.model.NotificationDto
import com.cm.demo.model.enums.CandidateStatus
import com.cm.demo.model.enums.TemplateName
import com.cm.demo.service.interfaces.*
import com.cm.demo.service.implementation.CandidateSearchRequest
import com.cm.demo.service.interfaces.CandidateService
import com.cm.demo.service.interfaces.*
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile

@Component
class CandidateEndpointImpl(
        private val candidateService: CandidateService,
        private val candidateDtoMapper: CandidateDtoMapper,
        private val universityExperienceEndpoint: UniversityExperienceEndpoint,
        private val jobExperienceEndPoint: JobExperienceEndPoint,
        private val emailNotificationService: EmailNotificationService
) : CandidateEndpoint {

    override fun getAll(name: String, pageable: Pageable): Page<CandidateDto> {
        val foundCandidates: Page<Candidate> = this.candidateService.findAll(name, pageable)
        val candidatesDto: Page<CandidateDto> = foundCandidates.map { this.candidateDtoMapper.convertToDto(it) }

        candidatesDto.map {
            it.universityExperience = this.universityExperienceEndpoint.getUniversityExperienceByCandidateId(it.id!!)
            it.jobExperience = this.jobExperienceEndPoint.getJobExperienceByCandidateId(it.id!!)
        }

        return candidatesDto
    }

    override fun getAllForPreview(pageable: Pageable): Page<CandidatePreviewDto> {
        val foundCandidates: Page<Candidate> = this.candidateService.findAll("", pageable)

        return foundCandidates.map { this.candidateDtoMapper.convertToCandidatePreviewDto(it) }
    }

    override fun getById(id: Long): CandidateDto {
        this.changeStatusToInProgress(id)

        val foundCandidate: Candidate = this.candidateService.findById(id)
        val candidateDto: CandidateDto = this.candidateDtoMapper.convertToDto(foundCandidate)

        candidateDto.universityExperience = getUniversityExperienceById(foundCandidate.id)
        candidateDto.jobExperience = getJobExperienceByCandidateId(foundCandidate.id)

        return candidateDto
    }

    override fun getByEmail(email: String): PublicCandidateDto {
        val candidate: Candidate = this.candidateService.findBydEmail(email)
        return candidateDtoMapper.convertToCandidateToPublicDto(candidate)
    }

    override fun add(createCandidateDto: CreateCandidateDto): CandidateDto {
        val candidate: Candidate = this.candidateDtoMapper.convertToEntity(createCandidateDto)
        val createdCandidate: Candidate = this.candidateService.add(candidate)

        val universityExperiencesDto: List<UniversityExperienceDto> = this.universityExperienceEndpoint
                .addAllUniversityExperience(createCandidateDto.createUniversityExperienceDto, createdCandidate)

        val jobExperiencesDto: List<JobExperienceDto> = this.jobExperienceEndPoint
                .addAllJobExperience(createCandidateDto.createJobExperienceDto, createdCandidate)

        val candidateDto: CandidateDto = this.candidateDtoMapper.convertToDto(createdCandidate)
        candidateDto.universityExperience = universityExperiencesDto
        candidateDto.jobExperience = jobExperiencesDto

        emailNotificationService.textSender(NotificationDto(candidate,
                null, "", "", TemplateName.NEW))

        return candidateDto
    }

    private fun getJobExperienceByCandidateId(id: Long?): List<JobExperienceDto>? {
        return this.jobExperienceEndPoint
                .getJobExperienceByCandidateId(id!!)
    }

    private fun getUniversityExperienceById(id: Long?): List<UniversityExperienceDto>? {
        return this.universityExperienceEndpoint
                .getUniversityExperienceByCandidateId(id!!)
    }

    @Transactional
    override fun changeStatusToInProgress(id: Long) {
        val candidate: Candidate = candidateService.findById(id)

        if (candidate.candidateStatus == CandidateStatus.NEW) {
            emailNotificationService.textSender(NotificationDto(candidate,
                    null, "", "", TemplateName.IN_PROGRESS))

            this.candidateService.updateStatus(id, CandidateStatus.IN_PROGRESS)
        }
    }

    override fun changeStatusToInvited(id: Long, date: String) {
        val candidate: Candidate = candidateService.findById(id)

        if (candidate.candidateStatus == CandidateStatus.IN_PROGRESS ||
                candidate.candidateStatus == CandidateStatus.SENT_TASK) {

            emailNotificationService.textSender(NotificationDto(candidate,
                    null, date, "", TemplateName.INVITED))

            this.candidateService.updateStatus(id, CandidateStatus.INVITED)
        }
        else {
            throw InvalidAdressException()
        }
    }


    override fun changeStatusToSentTask(id: Long, multipartFile: MultipartFile,
                                        date: String) {
        val candidate: Candidate = candidateService.findById(id)

        if (candidate.candidateStatus == CandidateStatus.IN_PROGRESS) {

            emailNotificationService.textSender(NotificationDto(candidate,
                    multipartFile, date, "", TemplateName.SENT_TASK))

            this.candidateService.updateStatus(id, CandidateStatus.SENT_TASK)
        }
        else {
            throw InvalidAdressException()
        }
    }

    override fun changeStatusToInReview(id: Long) {
        val candidate: Candidate = candidateService.findById(id)

        if (candidate.candidateStatus == CandidateStatus.INVITED) {

            emailNotificationService.textSender(NotificationDto(candidate,
                    null, "", "", TemplateName.IN_REVIEW))

            this.candidateService.updateStatus(id, CandidateStatus.IN_REVIEW)
        } else {
            throw InvalidAdressException()
        }
    }

    override fun changeStatusToDeclined(id: Long, hrText: String) {
        val candidate: Candidate = candidateService.findById(id)

        if (candidate.candidateStatus != CandidateStatus.ACCEPTED &&
                candidate.candidateStatus != CandidateStatus.DECLINED) {

            emailNotificationService.textSender(NotificationDto(candidate,
                    null, "", hrText, TemplateName.DECLINED))

            this.candidateService.updateStatus(id, CandidateStatus.DECLINED)
        } else {
            throw InvalidAdressException()
        }
    }

    override fun changeStatusToAccepted(id: Long, date: String) {
        val candidate: Candidate = candidateService.findById(id)

        if (candidate.candidateStatus == CandidateStatus.IN_REVIEW) {

            emailNotificationService.textSender(NotificationDto(candidate,
                    null, date, "", TemplateName.ACCEPTED))

            this.candidateService.updateStatus(id, CandidateStatus.ACCEPTED)
        } else {
            throw InvalidAdressException()
        }
    }

    override fun forceChangeCandidateStatus(id: Long, newStatus: CandidateStatus) {
        this.candidateService.forceUpdateStatus(id, newStatus)
    }

    override fun deleteCandidateById(id: Long) {
        this.candidateService.deleteCandidateById(id)
    }

    override fun search(searchRequest: CandidateSearchRequest, pageable: Pageable): Page<CandidatePreviewDto> {
        val candidates: Page<Candidate> =
                this.candidateService.search(searchRequest, pageable)

        return candidates.map { candidateDtoMapper.convertToCandidatePreviewDto(it) }
    }

}
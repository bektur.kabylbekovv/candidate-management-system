package com.cm.demo.endpoint.interfaces

import org.springframework.web.multipart.MultipartFile

interface PhotoEndpoint {

    fun addPhoto(candidateId: Long, candidateEmail: String, multipartFile: MultipartFile): String
}
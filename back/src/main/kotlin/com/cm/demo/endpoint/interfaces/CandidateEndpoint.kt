package com.cm.demo.endpoint.interfaces

import com.cm.demo.dto.implementation.candidate.CandidateDto
import com.cm.demo.dto.implementation.candidate.CreateCandidateDto
import com.cm.demo.dto.implementation.candidate.CandidatePreviewDto
import com.cm.demo.dto.implementation.candidate.PublicCandidateDto
import com.cm.demo.model.enums.CandidateStatus
import com.cm.demo.service.implementation.CandidateSearchRequest
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.web.multipart.MultipartFile

interface CandidateEndpoint {

    fun getAll(name: String, pageable: Pageable): Page<CandidateDto>
    fun getAllForPreview(pageable: Pageable): Page<CandidatePreviewDto>
    fun getById(id: Long): CandidateDto
    fun add(createCandidateDto: CreateCandidateDto): CandidateDto
    fun changeStatusToInvited(id: Long, date: String)
    fun changeStatusToSentTask(id: Long,  multipartFile: MultipartFile,
                               date: String)
    fun changeStatusToInReview(id: Long)
    fun changeStatusToDeclined(id: Long, hrText: String)
    fun changeStatusToAccepted(id: Long, date: String)
    fun changeStatusToInProgress(id: Long)
    fun deleteCandidateById(id: Long)
    fun forceChangeCandidateStatus(id: Long, newStatus: CandidateStatus)
    fun search(searchRequest: CandidateSearchRequest, pageable: Pageable): Page<CandidatePreviewDto>
    fun getByEmail(email: String): PublicCandidateDto
}
package com.cm.demo.endpoint.implementation

import com.cm.demo.dto.implementation.jobExperience.CreateJobExperienceDto
import com.cm.demo.dto.implementation.jobExperience.JobExperienceDto
import com.cm.demo.dto.interfaces.JobExperienceDtoMapper
import com.cm.demo.endpoint.interfaces.JobExperienceEndPoint
import com.cm.demo.model.Candidate
import com.cm.demo.model.JobExperience
import com.cm.demo.service.interfaces.JobExperienceService
import org.springframework.stereotype.Component

@Component
class JobExperienceEndPointImpl(
        private val jobExperienceService: JobExperienceService,
        private val jobExperienceDtoMapper: JobExperienceDtoMapper) : JobExperienceEndPoint {

    override fun getJobExperienceByCandidateId(id: Long): List<JobExperienceDto> {
        val foundJobExperiences: List<JobExperience> = this.jobExperienceService.findByCandidateId(id)
        return foundJobExperiences.map { this.jobExperienceDtoMapper.convertToDto(it) }
    }

    override fun addAllJobExperience(
            createJobExperiencesDto: List<CreateJobExperienceDto>, candidate: Candidate): List<JobExperienceDto> {

        val jobExperiences: List<JobExperience> =
                createJobExperiencesDto.map { this.jobExperienceDtoMapper.convertToEntity(it, candidate) }

        jobExperiences.map { it.candidate = candidate }

        val createdJobExperiencesDto: List<JobExperience> = this.jobExperienceService.addAll(jobExperiences)

        return createdJobExperiencesDto.map { jobExperienceDtoMapper.convertToDto(it) }
    }


}
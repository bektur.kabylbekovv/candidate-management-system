package com.cm.demo.endpoint.interfaces

import com.cm.demo.dto.implementation.unversity.CreateUniversityDto
import com.cm.demo.dto.implementation.unversity.UniversityDto
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable


interface UniversityEndpoint {

    fun getAll(name: String, pageable: Pageable): Page<UniversityDto>

    fun getById(id: Long): UniversityDto

    fun getAllByCountry(country: String, pageable: Pageable): Page<UniversityDto>

    fun add(createUniversityDto: CreateUniversityDto): UniversityDto

    fun addAll(createUniversitiesDto: List<CreateUniversityDto>): List<UniversityDto>
}
package com.cm.demo.endpoint.implementation

import com.cm.demo.dto.implementation.department.DepartmentDto
import com.cm.demo.dto.implementation.department.CreateDepartmentDto
import com.cm.demo.dto.interfaces.DepartmentDtoMapper
import com.cm.demo.endpoint.interfaces.DepartmentEndpoint
import com.cm.demo.model.Department
import com.cm.demo.service.interfaces.DepartmentService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component

@Component
class DepartmentEndpointImpl(
        private val departmentService: DepartmentService,
        private val departmentDtoMapper : DepartmentDtoMapper) : DepartmentEndpoint {

    override fun getAll(name: String, pageable: Pageable): Page<DepartmentDto> {
        val foundDepartments: Page<Department> = this.departmentService.findAll(name, pageable)
        return foundDepartments.map { departmentDtoMapper.convertToDto(it) }
    }

    override fun getById(id: Long): DepartmentDto {
        val foundDepartment: Department = this.departmentService.findById(id)
        return this.departmentDtoMapper.convertToDto(foundDepartment)
    }

    override fun add(createDepartmentDto: CreateDepartmentDto): DepartmentDto {
        val department: Department = this.departmentDtoMapper.convertToEntity(createDepartmentDto)
        val createdDepartment = this.departmentService.add(department)
        return this.departmentDtoMapper.convertToDto(createdDepartment)
    }

    override fun addAll(createDepartmentsDto: List<CreateDepartmentDto>): List<DepartmentDto> {
        val departments: List<Department> = createDepartmentsDto.map { this.departmentDtoMapper.convertToEntity(it) }
        val createdDepartments: List<Department> = this.departmentService.addAll(departments)
        return createdDepartments.map{ this.departmentDtoMapper.convertToDto(it) }
    }


}
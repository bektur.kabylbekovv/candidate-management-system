package com.cm.demo.endpoint.interfaces

import org.springframework.web.multipart.MultipartFile

interface TaskSolutionEndpoint {

    fun addSolution(candidateId: Long, candidateEmail: String, multipartFile: MultipartFile): String
}
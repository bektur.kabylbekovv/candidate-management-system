package com.cm.demo.endpoint.implementation

import com.cm.demo.endpoint.interfaces.PhotoEndpoint
import com.cm.demo.endpoint.interfaces.TaskSolutionEndpoint
import com.cm.demo.model.Candidate
import com.cm.demo.service.interfaces.AmazonService
import com.cm.demo.service.interfaces.CandidateService
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileOutputStream


@Component
class FileEndpointImpl(
        private val amazonService: AmazonService,
        private val candidateService: CandidateService) : PhotoEndpoint, TaskSolutionEndpoint {

    override fun addPhoto(candidateId: Long, candidateEmail: String, multipartFile: MultipartFile): String {
        val file: File = convertMultiPartToFile(multipartFile)
        val urlToGetPhoto: String =
                this.amazonService
                        .uploadPhotoFileToS3bucket(
                                candidateEmail,
                                multipartFile.originalFilename!!,
                                file)
        val candidate: Candidate =
                this.candidateService.findByIdAndEmail(candidateId, candidateEmail)
        candidate.photoUrl = urlToGetPhoto
        this.candidateService.add(candidate)

        file.delete()
        return urlToGetPhoto
    }

    override fun addSolution(candidateId: Long, candidateEmail: String, multipartFile: MultipartFile): String {
        println("LOG: " + candidateId)
        println("LOG: " + candidateEmail)
        println("LOG: " + multipartFile.originalFilename!!)
        val file: File = convertMultiPartToFile(multipartFile)

        val urlToGetSolution = this.amazonService.uploadSolutionFileToS3bucket(
                candidateEmail,
                multipartFile.originalFilename!!,
                file)
        println("URL GENERATED")
        val candidate: Candidate =
                this.candidateService.findByIdAndEmail(candidateId, candidateEmail)
        println("CANDIDATE FOUND")
        candidate.testTaskSolutionUrl = urlToGetSolution
        candidate.testTaskSolutionFileName = multipartFile.originalFilename
        this.candidateService.add(candidate)
        println("SOLUTION ATTACHED")
        file.delete()
        println(urlToGetSolution)
        return urlToGetSolution
    }

    private fun convertMultiPartToFile(multipartFile: MultipartFile): File {
        val file = File(multipartFile.originalFilename!!)
        val fileOutputStream = FileOutputStream(file)
        fileOutputStream.write(multipartFile.bytes)
        fileOutputStream.close()
        return file
    }

}
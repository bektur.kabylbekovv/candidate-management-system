package com.cm.demo.endpoint.interfaces

import com.cm.demo.dto.implementation.user.UserDto
import com.cm.demo.model.enums.Role
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface UserEndpoint {
    fun getAll(pageable: Pageable): Page<UserDto>
    fun changeRoleById(id: Long, newRole: Role)
    fun deleteUserById(id: Long)
}
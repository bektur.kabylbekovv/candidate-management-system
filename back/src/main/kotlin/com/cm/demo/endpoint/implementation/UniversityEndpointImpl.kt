package com.cm.demo.endpoint.implementation

import com.cm.demo.dto.implementation.unversity.CreateUniversityDto
import com.cm.demo.dto.implementation.unversity.UniversityDto
import com.cm.demo.dto.interfaces.UniversityDtoMapper
import com.cm.demo.endpoint.interfaces.UniversityEndpoint
import com.cm.demo.model.University
import com.cm.demo.service.interfaces.UniversityService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component

@Component
class UniversityEndpointImpl(
        private val universityService: UniversityService,
        private val universityDtoMapper : UniversityDtoMapper) : UniversityEndpoint {

    override fun getAll(name: String, pageable: Pageable): Page<UniversityDto> {
        val returnedUniversities: Page<University> = this.universityService.findAll(name, pageable)
        return returnedUniversities.map { universityDtoMapper.convertToDto(it) }
    }

    override fun getById(id: Long): UniversityDto {
        val returnedUniversity: University = this.universityService.findById(id)
        return this.universityDtoMapper.convertToDto(returnedUniversity)
    }

    override fun getAllByCountry(country: String, pageable: Pageable): Page<UniversityDto> {
        val returnedUniversities: Page<University> = this.universityService.findAllByCountry(country, pageable)
        return returnedUniversities.map { this.universityDtoMapper.convertToDto(it) }
    }

    override fun add(createUniversityDto: CreateUniversityDto): UniversityDto {
        val university: University = this.universityDtoMapper.convertToEntity(createUniversityDto)
        val returnedUniversity = this.universityService.add(university)
        return this.universityDtoMapper.convertToDto(returnedUniversity)
    }

    override fun addAll(createUniversitiesDto: List<CreateUniversityDto>): List<UniversityDto> {
        val universities: List<University> = createUniversitiesDto.map { this.universityDtoMapper.convertToEntity(it) }
        val returnedUniversities: List<University> = this.universityService.addAll(universities)
        return returnedUniversities.map{ this.universityDtoMapper.convertToDto(it) }
    }


}
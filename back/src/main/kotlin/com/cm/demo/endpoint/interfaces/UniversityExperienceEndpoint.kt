package com.cm.demo.endpoint.interfaces

import com.cm.demo.dto.implementation.universityExperience.CreateUniversityExperienceDto
import com.cm.demo.dto.implementation.universityExperience.UniversityExperienceDto
import com.cm.demo.model.Candidate

interface UniversityExperienceEndpoint {

    fun getUniversityExperienceByCandidateId(candidateId: Long): List<UniversityExperienceDto>

    fun addAllUniversityExperience(
            createUniversityExperiencesDto: List<CreateUniversityExperienceDto>,
            candidate: Candidate): List<UniversityExperienceDto>
}
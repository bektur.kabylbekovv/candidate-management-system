package com.cm.demo.endpoint.implementation

import com.cm.demo.dto.implementation.user.UserDto
import com.cm.demo.dto.interfaces.UserDtoMapper
import com.cm.demo.endpoint.interfaces.UserEndpoint
import com.cm.demo.model.enums.Role
import com.cm.demo.model.User
import com.cm.demo.service.interfaces.UserService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component

@Component
class UserEndpointImpl(
        private val userDetailsService: UserService,
        private val userDtoMapper: UserDtoMapper) : UserEndpoint {

    override fun deleteUserById(id: Long) {
        return this.userDetailsService.deleteUserById(id)
    }
    override fun changeRoleById(id: Long, newRole: Role) {
        return this.userDetailsService.changeRole(id, newRole)
    }
    override fun getAll(pageable: Pageable): Page<UserDto> {
        val users: Page<User> = this.userDetailsService.findAll(pageable)
        return users.map { this.userDtoMapper.convertToUserDto(it) }
    }

}
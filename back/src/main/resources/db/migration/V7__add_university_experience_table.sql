create table university_experience
(
    id            bigserial primary key not null,
    end_year      int8,
    faculty       varchar(255),
    start_year    int8,
    candidate_id  int8,
    university_id int8
)

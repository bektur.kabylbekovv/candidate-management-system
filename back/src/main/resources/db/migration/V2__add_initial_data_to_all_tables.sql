set search_path to public;

insert into departments (name) values ('Analyst');
insert into departments (name) values ('Project Manager');
insert into departments (name) values ('Quality Assurance');
insert into departments (name) values ('Java');
insert into departments (name) values ('JavaScript');
insert into departments (name) values ('Kotlin');
insert into departments (name) values ('Python');
insert into departments (name) values ('C#');
insert into departments (name) values ('C++');
insert into departments (name) values ('C');
insert into departments (name) values ('Assembler');
insert into departments (name) values ('Swift');
insert into departments (name) values ('Objective C');
insert into departments (name) values ('PHP');

insert into universities (country, name) values ('Kyrgyzstan', 'Bishkek State Economic and Commercial Institute');
insert into universities (country, name) values ('Kyrgyzstan', 'Eastern University named after Mahmud Kashgari Barskani');
insert into universities (country, name) values ('Kyrgyzstan', 'Institute of Management, Business and Tourism');
insert into universities (country, name) values ('Kyrgyzstan', 'International Atatürk-Alatoo University');
insert into universities (country, name) values ('Kyrgyzstan', 'International Academy of Management, Law, Finance and Business');
insert into universities (country, name) values ('Kyrgyzstan', 'International University of Central Asia');
insert into universities (country, name) values ('Kyrgyzstan', 'International University of Innovative Technologies');
insert into universities (country, name) values ('Kyrgyzstan', 'International University of Kyrgyzstan');
insert into universities (country, name) values ('Kyrgyzstan', 'International University of Science and Business');
insert into universities (country, name) values ('Kyrgyzstan', 'Kyrgyz Turkish Manas University');
insert into universities (country, name) values ('Kyrgyzstan', 'Kyrgyz Technical University');
insert into universities (country, name) values ('Kyrgyzstan', 'Kyrgyz State University of Construction, Transportation and Architecture');
insert into universities (country, name) values ('Kyrgyzstan', 'Kyrgyz State University of Arabaev');
insert into universities (country, name) values ('Kyrgyzstan', 'Kyrgyz-Russian Slavic University');
insert into universities (country, name) values ('Kyrgyzstan', 'Kyrgyz National University');

insert into universities (country, name) values ('Kyrgyzstan', 'American University of Central Asia');


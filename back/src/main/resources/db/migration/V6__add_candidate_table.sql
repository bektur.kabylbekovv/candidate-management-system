create table candidates
(
    id            bigserial primary key not null,
    email         varchar(255),
    constraint candidate_email unique (email),
    family_status varchar(25),
    gender        varchar(25),
    candidate_status varchar(50),
    name          varchar(255),
    birth_date    date,
    application_date date,
    phone         varchar(255),
    photo_url     varchar (2048),
    task_solution_url     varchar (2048),
    task_solution_file_name     varchar (2048),
    surname       varchar(255),
    department_id int8,
    skills  varchar(2048)
);


set search_path to public;

insert into templates (name, text) values ('NEW', 'Hello dear name_candidate,
You are successfully applied for the internship to the ZenSoft. Your application will be considered as soon as it is possible. Have a good time.

You can see your profile in http://localhost:3000/candidate/profile/candidate_id

Warmly, ZenSoft team

********************
');
insert into templates (name, text) values ('IN_PROGRESS', 'Hello dear name_candidate ,
Your application is on the stage of consideration. In the near future, you will be informed more about the details of your application. Have a good time.

You can see your profile in http://localhost:3000/candidate/profile/candidate_id

Warmly, Zensoft team

********************
');
insert into templates (name, text) values ('SENT_TASK', 'Hello dear name_candidate,
We need to estimate your skills. In the application of this message, you can find your test task. Deadline is givenDate. Have a good time.

You can see your profile in http://localhost:3000/candidate/profile/candidate_id

Warmly, ZenSoft team

********************
');
insert into templates (name, text) values ('DECLINED', 'Hello dear name_candidate,
Unfortunately, you are rejected from the internship program in ZenSoft, because text_hr. Don’t give up, you still will be in our list of potential candidates. Have a good time.

You can see your profile in http://localhost:3000/candidate/profile/candidate_id

Warmly, ZenSoft team

********************');
insert into templates (name, text) values ('INVITED', 'Hello dear name_candidate,
We are glad to invite you to the interview. Your interview is on givenDate. Have a good time.

You can see your profile in http://localhost:3000/candidate/profile/candidate_id

Warmly, ZenSoft team

********************
');
insert into templates (name, text) values ('IN_REVIEW', 'Hello dear name_candidate,
We are happy to inform you, that your application is almost fully considered. Soon, you will receive notification email about you further status. Have a good time.

You can see your profile in http://localhost:3000/candidate/profile/candidate_id

Warmly, ZenSoft team

********************
');
insert into templates (name, text) values ('ACCEPTED', 'Hello dear name_candidate,
CONGRATULATIONS!!! You are accepted to the internship program in ZenSoft. We will be waiting for you on givenDate. Please take replacement  shoes and cup. Have a good time.

You can see your profile in http://localhost:3000/candidate/profile/candidate_id

Warmly, ZenSoft team

********************');
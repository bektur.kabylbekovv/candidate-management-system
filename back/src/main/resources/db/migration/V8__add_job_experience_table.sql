create table job_experience(
    id           bigserial primary key not null,
    company      varchar(255),
    description  varchar(2048),
    end_year     int8,
    job_title    varchar(255),
    start_year   int8,
    candidate_id int8
)

create table new_users (
    id          bigserial primary key,
    first_Name   varchar(255),
    last_Name    varchar(255),
    email        varchar(255),
    role         varchar(25),
    phone_Number varchar(255),
    password     varchar(255),
    constraint email_index unique(email)
);
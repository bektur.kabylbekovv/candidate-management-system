create table templates (
    id          bigserial primary key,
    name    varchar(255),
    text    text
)
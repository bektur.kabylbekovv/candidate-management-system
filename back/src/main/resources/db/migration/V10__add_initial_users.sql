insert into new_users(
    email, first_name, last_name, password, phone_number, role) values
    ('shostak_d@auca.kg', 'Dmitriy', 'Shostak', '$2a$10$LyJRL7bF96vEuwi88QAk/eWk37YMeiwA4pfNlAuej35SueL6ER6Be', '996 123 456 789', 'INTERVIEWER');
insert into new_users(
    email, first_name, last_name, password, phone_number, role) values
    ('toksaitov_d@auca.kg', 'Dmitriy', 'Toksaitov', '$2a$10$LyJRL7bF96vEuwi88QAk/eWk37YMeiwA4pfNlAuej35SueL6ER6Be', '996 987 654 321', 'HR')
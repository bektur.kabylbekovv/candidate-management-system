set search_path to public;

create sequence hibernate_sequence start 1 increment 1;

create table departments
(
    id   bigserial primary key,
    name varchar(255),
    constraint department_name_constraint unique(name)
);

create table universities
(
    id      bigserial primary key,
    country varchar(255),
    name    varchar(255)
);

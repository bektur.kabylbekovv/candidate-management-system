create table skills (
    id bigserial primary key not null,
    candidate_id int8,
    skill_name varchar(255)
)